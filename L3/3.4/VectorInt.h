#ifndef VECTORINT_H_INCLUDED
#define VECTORINT_H_INCLUDED

#include <iostream>
//using namespace std;

class VectorInt
 {
 public:
    VectorInt();//konstruktor na 16 intow OK
    VectorInt(int);//konstruktor z argumentem
    VectorInt(const VectorInt&);//konstruktor kopiujacy, codeblock podpowiada const CectorInt& nieuzywany
    VectorInt& operator= (const VectorInt& );//operator przypisania nieuzywany
    ~VectorInt();//destruktor OK
    int at(int);//zwraca wartosc na indeksie int OK
    void insert(int,int);//dodaje(index, value) nowy element OK
    void pushBack(int);//dodaje element na koncu, ew powieksza pamiec OK
    void popBack();//usuwa ostatni element ciagu OK
    void shrinkToFit();//dostosowuje alokacje pamieci OK
    void clear();//czysci wszyskie inty wektora OK
    int size();//zwraca dlugosc wektora OK
    int capacity();//sprawdza ile jescze wejdzie bez alokacji OK
    friend std::ostream & operator << (std::ostream &out,VectorInt &vec);

private:
     int *vec;
     int length;//dlugosc calej tablicy
     int veclength;//dlugosc wektora
     int atforop(int);
 };

std::ostream& operator<< (std::ostream &out, VectorInt &vec);
#endif // VECTORINT_H_INCLUDED
