#include "VectorInt.h"
#include <iostream>
#include <string>
#include <sstream>
VectorInt::VectorInt()//OK
{
    length=16;
    veclength=0;
    vec= new int[length];
    for(int i=0; i<length; i++)
    {
        vec[i]=0;
    }
}


VectorInt::VectorInt(int n)//OK
{
    if(n>0)
    {
        length=n;
        veclength=0;
        vec= new int[length];
        for(int i=0; i<length; i++)
        {
            vec[i]=0;
        }
    }
    else
    {
        length=1;
        veclength=0;
        vec= new int[1];
        vec[0]=0;
    }
}


VectorInt::VectorInt(const VectorInt& newvec)
{
    this->length=newvec.length;
    this->veclength=newvec.veclength;
    vec= new int[length];
    for(int i=0; i<(this->length); i++)\
    {
        this->vec[i]=newvec.vec[i];
    }
}


VectorInt& VectorInt::operator= (const VectorInt& newvec)
{
    if(this!=&newvec)
    {
        delete this->vec;
        this->length=newvec.length;
        this->veclength=newvec.veclength;
        vec= new int[length];
        for(int i=0; i<(this->length); i++)\
        {
            this->vec[i]=newvec.vec[i];
        }
    }
    return *this;
}


VectorInt::~VectorInt()//OK
{
    length=0;
    veclength=0;
    delete[]vec;
}


int VectorInt::at(int index)//OK
{
    if(index>=0&&index<veclength)
    {
        std::cout<<"na indeksie "<<index<<" jest wartosc "<<vec[index]<<std::endl;
        return vec[index];
    }
    else
    {
        std::cout<<"zly indeks, wektor nie ma takich skladowych"<<std::endl;
        return false;
    }
}


void VectorInt::insert(int index, int value)//OK
{
    if(index<0)
    {
        std::cout<<"zly indeks"<<std::endl;
    }
    else if(index>=length)
    {
        int tab[index+1];
        for(int i=0; i<length; i++)
        {
            tab[i]=vec[i];
        }
        delete[]vec;
        vec=new int[(index+1)];
        for(int i=0; i<length; i++)
        {
            vec[i]=tab[i];
        }
        for(int i =(length+1); i<(index); i++)
        {
            vec[i]=0;
        }
        vec[index]=value;
        length=index+1;
        veclength=length;
        std::cout<<"na indeksie "<<index<<" zostala dodana wartosc "<<vec[index]<<std::endl;
    }
    else
    {
        vec[index]=value;
        if(veclength<=index)
        {
            veclength=index+1;
        }
        std::cout<<"na indeksie "<<index<<" zostala dodana wartosc "<<vec[index]<<std::endl;
    }
}


void VectorInt::pushBack(int value)//OK
{
    if(veclength==length)
    {
        length++;
        int tab[length];
        for(int i=0; i<length; i++)
        {
            tab[i]=vec[i];
        }
        delete[]vec;
        vec = new int[length];
        for(int i=0; i<length; i++)
        {
            vec[i]=tab[i];
        }
        vec[length-1]=value;
        veclength=length;
    }
    else
    {
        vec[veclength]=value;
        veclength++;
    }
}


void VectorInt::popBack()//OK
{
    if(veclength>0)
    {
        vec[veclength-1]=0;
        int i=veclength-1;
        while(i>=0)
        {
            if(vec[i]!=0)
            {
                break;
            }
            i--;
        }
        veclength=i+1;
    }
    else
    {
        std::cout<<"wektor zerowy, nie ma czego usuwac"<<std::endl;
    }
}


void VectorInt::shrinkToFit()//OK
{
    if(veclength==0)
    {
        delete[]vec;
        vec=new int[1];
        vec[0]=0;
        veclength=0;
        length=1;
    }
    else if(length>veclength)
    {
        int tab[veclength];
        for(int i=0; i<veclength; i++)
        {
            tab[i]=vec[i];
        }
        delete[]vec;
        vec=new int[veclength];
        for(int i=0; i<veclength; i++)
        {
            vec[i]=tab[i];
        }
        length=veclength;
        std::cout<<"pamiec dostosowana"<<std::endl;
    }
    else
    {
        std::cout<<"nie ma takiej potrzeby"<<std::endl;
    }
}


void VectorInt::clear()//OK
{
    for (int i=0; i<length; i++)
    {
        vec[i]=0;
    }
    veclength=0;
    std::cout<<"wektor wyczyszczony"<<std::endl;
}

int VectorInt::size()//OK
{
    std::cout<<"dlugosc wektora: "<<veclength<<std::endl;
    return veclength;
}


int VectorInt::capacity()//OK
{
    std::cout<<"wolne miejsce "<<(length-veclength)<<std::endl;
    return (length-veclength);
}

int VectorInt::atforop(int index)
{
    return vec[index];
}


using namespace std;
ostream& operator<< (ostream &out,VectorInt &vec)
{
    if(vec.length>0)
    {
        string print = "[";
        for(int i = 0; i<vec.length-1; i++)
        {
            stringstream ss;
            ss<<vec.atforop(i);
            string s;
            ss>>s;
            print = print + s +",";
        }
        stringstream ss;
        ss<<vec.atforop(vec.length-1);
        string s;
        ss>>s;
        print = print + s +"]";
        return out<<print;
    }
}















