/*point2d.cpp*/
/*Plik �r�d�owy klasy Point2d.hpp*/
#include"point2d.hpp"
#include<iostream>
#include<cmath>
#include<math.h>
#define PI 4*atan(1.)


//konstruktor bez argument�w zwraca punkt (0,0)
//wykorzystano konstruktor z list� inicjalizacyjn�
Point2d::Point2d()
{
    _x=0;
    _y=0;
    _r=0;
    _phi=0;
}

//konstruktor pobieraj�cy wsp�rz�dne
//wykorzystano konstruktor z list� inicjalizacyjn�
Point2d::Point2d(double x, double y)
{
    _x=0;
    _y=0;
    _r=sqrt(_x*_x+_y*_y);
    _phi=atan2(_y,_x);
}

//konstruktor kopiuj�cy
//wykorzystano konstruktor z list� inicjalizacyjn�
Point2d::Point2d(const Point2d& other)
{
    _x = other._x;
    _y = other._y;
    _r = other._r;
    _phi = other._phi;
}

//przeci��ony operator przypisania
Point2d& Point2d::operator= (const Point2d& other)
{
    //wykorzystano wska�nik this pokazuj�cy
    //na "ten" obiekt
    this->_x = other._x;
    this->_y = other._y;
    this->_r = other._r;
    this->_phi = other._phi;
    //operator zwraca "ten" obiekt, aby mo�na by�o
    //wykona� wielokrotne przypisanie
    return *this;
}

//wsp�rz�dna x
double Point2d::getX()
{
    return _x;
}

//wsp�rz�dna y
double Point2d::getY()
{
    return _y;
}

//wsp�rz�dna r
double Point2d::getR()
{
    return _r;
}

//wsp�rz�dna phi
double Point2d::getPhi()
{
    return _phi;
}

void Point2d::setXY(double x, double y)
{
    //przypisanie z wykorzystaniem wska�nika this oraz wprost
    this->_x = x;
    this->_y = y;
    _r=sqrt(_x*_x+_y*_y);
    _phi=atan2(_y,_x);
}

void Point2d::setRPhi(double r, double phi)
{
        _phi = fmod(phi, 2*PI);
    _x = r*cos(phi);
    _y = r*sin(phi);
    _r = r;

}
void Point2d::dilate(double k)
{
    setXY(_x*k,_y*k);
}
void Point2d::rotation(double alfa)
{
    _phi = _phi+alfa;
    setRPhi(_r,_phi);
}

//przeci��ony operator<< dla wypisywania
std::ostream& operator<<(std::ostream& out, Point2d& p)
{
    return out << "[" << p.getX() << ", " << p.getY() << "]";
}
