/*main.cpp*/
/*Program demonstruj�cy mo�liwo�ci
klasy Point2d.*/
#include <iostream>
#include "point2d.hpp"
#include <cstdio>
#include <windows.h>
using namespace std;

int main()
{
    //obiekt a utworzony z wykorzystaniem konstruktura domy�lnego
    Point2d a;
    int k;
    int alfa;
    char choice;
    //do wy�wietlania wykorzystujemy przeci��ony operator <<
    //std::ostream& operator<<(std::ostream& out, Point2d& p);
    //cout << "a = " << a << endl;
    //metoda setXY ustawia wsp�rz�dne punktu
    a.setXY(3, 4);


    /*//obiekt b utworzony z wykorzystaniem konstruktora pobieraj�cego
    //dwie liczby - wsp�rz�dne punktu
    Point2d b(-2,-1);
    cout << "b = " << b << endl;

    //obiekt c utworzony z wykorzystaniem konstruktora kopiuj�cego
    Point2d c(b);
    cout << "c = " << c << endl;

    //wykorzystanie metod get do odczytania wsp�rz�dnych punktu
    cout << "a.x = "   << a.getX() << endl;
    cout << "a.y = "   << a.getY() << endl;
    cout << "a.r = "   << a.getR() << endl;
    cout << "a.phi = " << a.getPhi() << endl;

    //metoda setRPhi ustawia wsp�rz�dne punktu na podstawie
    //wsp�rz�dnych biegunowych
    b.setRPhi(5,1);
    cout << "b = " << b << endl;*/

    while(1)
    {
        cout << "a = " << a << endl;
        cout<<"phi"<< a.getPhi() <<endl;
        cout<<"1. dilate"<<endl<<"2. rotation"<<endl;
        cin>>choice;
        switch(choice)
        {
        case '1':
        {
            cout<<"dilate: choose k"<<endl;
            cin>>k;
            a.dilate(k);
            break;
        }
        case '2':
        {
            cout<<"rotation: choose alfa [radians]"<<endl;
            cin>>alfa;
            a.rotation(alfa);
            break;
        }
        }
        system("cls");
    }
    return 0;
}

