#include <iostream>
#include <windows.h>

using namespace std;
void swapPlayers(char &player)
{
    if (player == 'X')
        player = 'O';
    else
        player = 'X';
}
void printTAB(char tab[3][3])
{
    for(int i=0; i<3; i++)
    {
        cout<<endl<<"+---+---+---+"<<endl<<"| ";
        for(int j=0; j<3; j++)
        {
            cout<<tab[i][j]<<" |"<<" ";
        }
    }
    cout<<endl<<"+---+---+---+";
}
int checkSign(int number,char tab[3][3])
{
    if(tab[number/3][number%3-1]=='X'||tab[number/3][number%3-1]=='O')
    {
        return 1;
    }
    else return 0;
}
void add(int number,char tab[3][3],char sign)
{
    tab[number/3][number%3-1]= sign;
}
int checkWin(char tab[3][3])
{
    if((tab[0][0]=='X'&&tab[0][1]=='X'&&tab[0][2]=='X')||
       (tab[1][0]=='X'&&tab[1][1]=='X'&&tab[1][2]=='X')||
       (tab[2][0]=='X'&&tab[2][1]=='X'&&tab[2][2]=='X')||
       (tab[0][0]=='X'&&tab[1][0]=='X'&&tab[2][0]=='X')||
       (tab[0][1]=='X'&&tab[1][1]=='X'&&tab[2][1]=='X')||
       (tab[0][2]=='X'&&tab[1][2]=='X'&&tab[2][2]=='X')||
       (tab[0][0]=='X'&&tab[1][1]=='X'&&tab[2][2]=='X')||
       (tab[0][2]=='X'&&tab[1][1]=='X'&&tab[2][0]=='X'))
    {
        return 1;
    }
    else if((tab[0][0]=='O'&&tab[0][1]=='O'&&tab[0][2]=='O')||
       (tab[1][0]=='O'&&tab[1][1]=='O'&&tab[1][2]=='O')||
       (tab[2][0]=='O'&&tab[2][1]=='O'&&tab[2][2]=='O')||
       (tab[0][0]=='O'&&tab[1][0]=='O'&&tab[2][0]=='O')||
       (tab[0][1]=='O'&&tab[1][1]=='O'&&tab[2][1]=='O')||
       (tab[0][2]=='O'&&tab[1][2]=='O'&&tab[2][2]=='O')||
       (tab[0][0]=='O'&&tab[1][1]=='O'&&tab[2][2]=='O')||
       (tab[0][2]=='O'&&tab[1][1]=='O'&&tab[2][0]=='O'))
    {
        return 1;
    }
  else return 0;
}


int main()
{
    char tab[3][3]={{'1','2','3'},{'4','5','6'},{'7','8','9'}};
    printTAB(tab);
    char player = 'X';
    int wynik=0;
    int checking=0;
    while(1)
    {
        int number;
        cout<<endl<<endl<<"Gracz "<<player<<endl;
        cout<<"Podaj nr"<<endl;
        cin>>number;
        while(number<0||number>9)
        {
            cout<<"Podaj liczbe w zakresie 1-9"<<endl;
            cin>>number;
            system("cls");
        }
        checking=checkSign(number, tab);
        while(checking==1)
        {
            cout<<"Podaj inny nr"<<endl;
            cin>>number;
            checking=checkSign(number, tab);
        }
        add(number,tab,player);
        system("cls");
        printTAB(tab);
        wynik=checkWin(tab);
        if(wynik==1)
        {
            break;
        }
        swapPlayers(player);

    }
    cout<<endl<<"Wygral gracz "<<player;
    return 0;
}
