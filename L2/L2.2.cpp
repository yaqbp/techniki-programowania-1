#include<iostream>
#include<iomanip>
using namespace std;
void printTAB(int **tab,int n)
{
    for(int i=0; i<n; i++)
    {
        cout<<endl;
        for(int j=0; j<n; j++)
        {
            cout<<setw(3)<<tab[i][j]<<" " ;
        }
    }
    cout<<endl;
}
int main()
{
    int n;
    cout<<"Podaj liczbe naturalna"<<endl;
    cin>>n;
    int ** tab = new int * [n];
    for (int i = 0; i<n; i++)
        tab[i] = new int [n];
    for(int i=0; i<n; i++)
    {
        tab[i][0]=i+1;
    }
    for(int i=0; i<n; i++)
    {
        tab[0][i]=i+1;
    }
    //printTAB(tab, n);
    for(int i=1; i<n; i++)
    {
        for(int j=1; j<n; j++)
        {
            tab[i][j]=tab[0][j]*tab[i][0];
        }

    }
    printTAB(tab, n);
    cout<<endl;
    delete [] tab;
}
