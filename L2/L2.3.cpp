#include<iostream>
#include<iomanip>
using namespace std;
int* makeTableGreatAgain(int* tab, int &lenght)
{
    int* newtab = new int[lenght+1];
    for (int i=0; i<lenght; i++)
    {
        newtab[i]=tab[i];
    }
    lenght++;
    delete[]tab;
    return newtab;
}
void printdynamicTAB(int **tab,int n)
{
    for(int i=1; i<n; i++)
    {
        cout<<endl;
        for(int j=0; j<n; j++)
        {
            if(tab[i][j]==0)
            {
                break;
            }
            else
            cout<<setw(3)<<tab[i][j]<<" " ;
        }
    }
    cout<<endl;
}

int main()
{
    int n;
    int p;
    cout<<"Podaj liczbe naturalna"<<endl;
    cin>>n;
    while(n<=0)
    {
        cout<<"Pokaze swoje mozliwosci tylko jesli podasz wieksza liczbe"<<endl;
        cin>>n;
    }
    int** tab = new int* [n];
    int lenght;
    for (int i=1; i<=n; i++)
    {
        lenght=1;
        tab[i]=new int[1];
        tab[i][0]=i;
        cout<<"Liczba: "<<tab[i][0]<<" - dzielniki liczby:";
        for(int j=1;j<=i;j++)
        {
            if(i%j==0)
            {
                tab[i]=makeTableGreatAgain(tab[i],lenght);
                tab[i][lenght-1]=j;
                cout<<setw(2)<<tab[i][lenght-1]<<", ";
            }
        }
        tab[i]=makeTableGreatAgain(tab[i],lenght);
        tab[i][lenght-1]=0;//dodaje zero na koncu by program wiedzial, ze tablica sie juz konczy
        cout<<endl;
    }
    cout<<endl<<"By wyswietlic tablice nacisnij 1"<<endl;
    cin>>p;
    if(p==1)
    {
        cout<<endl<<"Oto tablica w ktorej zapisywalem liczby i dzielniki"<<endl;
        printdynamicTAB(tab,n+1);
    }
    for (int i=0; i<n; i++)
    {
        delete[] tab[i];
    }
    delete[] tab;
}
